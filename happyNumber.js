const happyNumber = (n, counter = 0) => {
  output = false;
  if (counter < 10) {
    let input = n
      .toString()
      .split("")
      .map((n) => n * n);
    let result = input.reduce((a, b) => a + b, 0);
    if (result === 1) {
      return (output = true);
    } else {
      counter++;
      happyNumber(result, counter);
    }
  }
  return output;
};
console.log(happyNumber(68));
